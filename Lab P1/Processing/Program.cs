﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using static Processing.HelperFunctions;
using System.Linq;

namespace Processing
{
    internal static class Program
    {
        private static readonly SQLiteConnection OriginalDb = CreateConnection("../Preprocessing/original");
        private static readonly SQLiteConnection MetaDb = CreateConnection("../Preprocessing/metadb");
        
        public static void Main(string[] args)
        {
            while (RunProgram())
                continue;
            
            OriginalDb.Close();
            MetaDb.Close();
        }

        /// <summary>
        /// Ask for and handle a query. This can be repeatedly called to process multiple queries consecutively.
        /// </summary>
        /// <returns>False if the user submits "exit", otherwise true.</returns>
        private static bool RunProgram()
        {
            // Ask for query, terminate if "exit".
            Console.WriteLine("Query input:");
            string query = Console.ReadLine();
            if (query == "exit")
                return false;
            
            // Convert query to a data structure we can work with.
            (Dictionary<string, object>request, uint k) = ConvertQuery(query);
            
            // Execute the query.
            ExecuteRequest(request, k);
            
            return true;
        }

        private static void ExecuteRequest(Dictionary<string, object> request, uint k)
        {
            string selectAll = "SELECT * FROM autompg;";
            SQLiteCommand command = new SQLiteCommand(selectAll, OriginalDb);
            SQLiteDataReader reader = command.ExecuteReader();
            string[] categorical = {"origin", "brand", "model", "type"};
            Dictionary<int, double> allTuples = new Dictionary<int, double>();
            while (reader.Read())
            {
                long id = (long)reader["id"];
                double score = 0;
                foreach (KeyValuePair<string, object> term in request)
                {
                    if (!categorical.Contains(term.Key))
                        score += NumericalScore(reader[term.Key], term);
                    else if (reader[term.Key].Equals(term.Value))
                        score += CategoricalScore(reader[term.Key], term);
                }
                allTuples.Add((int)id, score);
            }

            IOrderedEnumerable<KeyValuePair<int, double>> sortedDict = from entry in allTuples orderby entry.Value descending select entry;
            IEnumerable<KeyValuePair<int, double>> topk = sortedDict.Take((int)k);
            
            int counter = 1;
            foreach (KeyValuePair<int, double> t in topk)
            {
                string sql = $"SELECT * FROM autompg WHERE id = {t.Key};";
                command = new SQLiteCommand(sql, OriginalDb);
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"#{counter} - " +
                                      $"id: {reader["id"]}," +
                                      $" mpg: {reader["mpg"]}," +
                                      $" cyl: {reader["cylinders"]}," +
                                      $" disp: {reader["displacement"]}," +
                                      $" hp: {reader["horsepower"]}," +
                                      $" w: {reader["weight"]}," +
                                      $" acc: {reader["acceleration"]}," +
                                      $" year: {reader["model_year"]}," +
                                      $" org: {reader["origin"]}," +
                                      $" brand: {reader["brand"]}," +
                                      $" model: {reader["model"]}," +
                                      $" type: {reader["type"]}," +
                                      $" score: {(float)t.Value}");
                    counter++;
                }
            }
        }

        private static double NumericalScore(object t, KeyValuePair<string, object> q)
        {
            double h = GetBandwidth(q.Key);
            double idf = CalcNumIdf(q.Key, Convert.ToDouble(q.Value), h);
            double value = Convert.ToDouble(t);
            
            double score = Math.Exp(-0.5*Math.Pow((value - Convert.ToDouble(q.Value)) / h, 2)) * idf;
            
            return score;
        }

        private static double CalcNumIdf(string attribute, double value, double h)
        {
            double sum = HelperFunctions.CalculateGaussSum(OriginalDb, attribute,
                value, h);
            
            string sql = $"SELECT * FROM misc;";
            SQLiteCommand command = new SQLiteCommand(sql, MetaDb);
            SQLiteDataReader reader = command.ExecuteReader();
            reader.Read();
            double n = (double)(long)reader["totaln"];

            return Math.Log(n / sum);
        }

        private static double GetBandwidth(string qKey)
        {
            string sql = $"SELECT * FROM bandwidths WHERE attribute = '{qKey}';";
            SQLiteCommand command = new SQLiteCommand(sql, MetaDb);
            SQLiteDataReader reader = command.ExecuteReader();
            double bandwidth = 0;

            if (reader.Read())
                bandwidth = (double)reader["bandwidth"];

            return bandwidth;
        }


        private static double CategoricalScore(object t, KeyValuePair<string, object> term)
        {
            double idf = GetIdf(term);
            double qf = GetQf(term);
            
            if (term.Key == "cylinders" || term.Key == "model_year" || term.Key == "origin")
                return idf * qf;
            else
            {
                string sql = $"SELECT * FROM {term.Key}_w WHERE value = '{(string)t}';";
                SQLiteCommand command = new SQLiteCommand(sql, MetaDb);
                SQLiteDataReader reader = command.ExecuteReader();
                string[] v = {""};
                if (reader.Read())
                    v = ((string)reader["querysets"]).Split(',');
                
                HashSet<string> setT = new HashSet<string>(v);
                
                sql = $"SELECT * FROM {term.Key}_w WHERE value = '{(string)term.Value}';";
                command = new SQLiteCommand(sql, MetaDb);
                reader = command.ExecuteReader();
                v = new []{""};
                if (reader.Read())
                    v = ((string)reader["querysets"]).Split(',');
                
                HashSet<string> setQ = new HashSet<string>(v);
                
                return Jaccard(setT, setQ) * qf;
            }
        }

        private static double GetIdf(KeyValuePair<string, object> term)
        {
            string sql = $"SELECT * FROM {term.Key}_idf WHERE value = '{term.Value}';";
            SQLiteCommand command = new SQLiteCommand(sql, MetaDb);
            SQLiteDataReader reader = command.ExecuteReader();
            double idf = 0;

            if (reader.Read())
            {
                idf = (double)reader["idf"];
            }

            return idf;
        }
        
        private static double GetQf(KeyValuePair<string, object> term)
        {
            string sql = $"SELECT * FROM {term.Key}_qf WHERE value = '{term.Value}';";
            SQLiteCommand command = new SQLiteCommand(sql, MetaDb);
            SQLiteDataReader reader = command.ExecuteReader();
            double qf = 0;

            if (reader.Read())
            {
                qf = (double)reader["qf"];
            }
            else
            {
                string sql2 = $"SELECT * FROM misc;";
                SQLiteCommand command2 = new SQLiteCommand(sql2, MetaDb);
                SQLiteDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                long rqfmax = (long)reader2["rqfmax"];
                qf = 1 / (double)(rqfmax + 1);
            }

            return qf;
        }

        // Index-based Threshold Algorithm
        private static void ITA()
        {
            
        }

        
        // All code below here is not used in the final implementation.
        // It was written to be able to implement the Index-based Threshold Algorithm,
        // but it was not finished in time.
        private static void AddIndexes(SQLiteConnection connection)
        {
            string[] attributes = {"mpg", "cylinders", "displacement", "horsepower", "weight", "acceleration",
                "model_year", "origin", "brand", "model", "type"};
            foreach (string attribute in attributes)
            {
                string sql = $"CREATE INDEX idx_{attribute} ON autompg({attribute});";
                SQLiteCommand command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
            }
        }
        
        private static Dictionary<string, object> TupleLookup(string id)
        {
            Dictionary<string, object> tupleData = new Dictionary<string, object>();

            SQLiteConnection connection = CreateConnection("original");
            connection.Open();

            string sql = $"SELECT * FROM autompg WHERE id = {id}";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        tupleData[reader.GetName(i)] = reader.GetValue(i);
                    }
                }
            }

            return tupleData;
        }

        private static int IndexLookupGetNextId(List<int> ordering, int currentIndex)
        {
            int nextId = -1;
            if (currentIndex < ordering.Count - 1)
                nextId = ordering[currentIndex + 1];

            return nextId;
        }
        
        private static List<int> GetOrdering()
        {
            List<int> ids = new List<int>();
            
            SQLiteConnection connection = CreateConnection("original");
            connection.Open();

            string sql = $"SELECT id FROM autompg ORDER BY CASE WHEN Ak = qk THEN 1 ELSE 2 END;";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    ids.Add((int)reader["id"]);
                }
            }

            return ids;
        }
    }
} 