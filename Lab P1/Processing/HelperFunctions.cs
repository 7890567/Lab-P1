using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace Processing
{
    public static class HelperFunctions
    {
        /// <summary>
        /// Create a connection to a database.
        /// </summary>
        /// <param name="name">The name/path of the database file, relative to the Processing root, and assumed to have a .sqlite extension.</param>
        public static SQLiteConnection CreateConnection(string name)
        {
            SQLiteConnection connection = new SQLiteConnection($"Data Source=../../{name}.sqlite;Version=3;");
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Parse an attribute value to the correct type.
        /// </summary>
        /// <param name="attribute">The attribute that the value belongs to.</param>
        /// <param name="value">The value in string representation.</param>
        /// <returns>The value parsed to the correct type, based on the attribute, given as type object.</returns>
        /// <exception cref="ArgumentException">The attribute is not valid.</exception>
        public static object ParseAttribute(string attribute, string value)
        {
            switch (attribute)
            {
                // type double
                case "mpg":
                case "displacement":
                case "horsepower":
                case "weight":
                case "acceleration":
                    return double.Parse(value);
                // type long
                case "cylinders":
                case "model_year":
                case "origin":
                    return long.Parse(value);
                // type string
                case "brand":
                case "model":
                case "type":
                    return value.Trim('\'');
            }

            throw new ArgumentException("The provided attribute name is not valid.", nameof(attribute));
        }

        /// <summary>
        /// Convert an input query into a dictionary storing the requested values and extract a value for k.
        /// </summary>
        /// <param name="query">The string representing a query.</param>
        /// <returns>The requested values and the value of k.</returns>
        public static (Dictionary<string, object>, uint) ConvertQuery(string query)
        {
            // Setup
            Dictionary<string, object> result = new Dictionary<string, object>();
            uint k = 10;
            string[] attrValues = query.TrimEnd(';').Split(new[] { ", " }, StringSplitOptions.None);
            
            // For each requested value, add it to the dictionary or update the value of k.
            foreach (string attrValue in attrValues)
            {
                string[] attrValueSplit = attrValue.Split(new[] { " = " }, StringSplitOptions.None);
                if (attrValueSplit[0] == "k")
                    k = uint.Parse(attrValueSplit[1]);
                else
                    result[attrValueSplit[0]] = ParseAttribute(attrValueSplit[0], attrValueSplit[1]);
            }

            return (result, k);
        }
    
        public static double CalculateGaussSum(SQLiteConnection connection, string attribute, double t, double bandwidth)
        {
            string sql = $"SELECT {attribute} FROM autompg;";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            double sum = 0.0;

            while (reader.Read())
            {
                double t_i = Convert.ToDouble(reader[0]);
                sum += Math.Exp(-0.5 * Math.Pow((t_i - t) / bandwidth, 2));
            }

            return sum;
        }
        
        public static double Jaccard(HashSet<string> setT, HashSet<string> setQ)
        {
            int intersection = setT.Intersect(setQ).Count();
            int union = setT.Union(setQ).Count();
            if (union == 0)
                return 1;

            return (double)intersection / union;
        }
    }
}