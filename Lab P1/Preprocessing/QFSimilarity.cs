﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static Preprocessing.HelperFunctions;

namespace Preprocessing
{
    public class QFSimilarity
    {
        public Dictionary<string, Dictionary<object, uint>> RQFrequencies =
            new Dictionary<string, Dictionary<object, uint>>(Program.Attributes.Length);
        
        public Dictionary<string, Dictionary<object, double>> QFrequencies =
            new Dictionary<string, Dictionary<object, double>>(Program.Attributes.Length);

        public Dictionary<string, Dictionary<string, HashSet<uint>>> QuerySets =
            new Dictionary<string, Dictionary<string, HashSet<uint>>>(Program.Attributes.Length);

        public uint RQFMax { get; }

        public QFSimilarity(string workload)
        {
            foreach (string attr in Program.Attributes)
            {
                RQFrequencies.Add(attr, new Dictionary<object, uint>());
                QuerySets.Add(attr, new Dictionary<string, HashSet<uint>>());
            }
            
            IEnumerable<string> lines = File.ReadLines(workload);

            lines = lines.Skip(2);

            uint queryCounter = 0;
            foreach (string line in lines)
            {
                if (line == "")
                    break;
                
                queryCounter++;
                string[] parts = line.Split(new[] { " WHERE " }, StringSplitOptions.None);
                uint frequency = uint.Parse(parts[0].Split(' ')[0]);

                string[] conditions = parts[1].Split(new[] { " AND " }, StringSplitOptions.None);
                foreach (string condition in conditions)
                {
                    if (condition.Contains("="))
                    {
                        string[] words = condition.Split(' ');
                        string attribute = words[0];
                        string value = words[2].Trim('\'');
                        IncreaseCount(attribute, value, frequency);
                    }
                    else
                    {
                        string[] parts2 = condition.Split(new[] { " IN " }, StringSplitOptions.None);
                        string attribute = parts2[0];
                        string[] values = parts2[1].TrimStart('(', '\'').TrimEnd(')', '\'')
                            .Split(new[] { "','" }, StringSplitOptions.None);
                        foreach (string value in values)
                        {
                            IncreaseCount(attribute, value, frequency);

                            if (!QuerySets[attribute].ContainsKey(value))
                            {
                                QuerySets[attribute][value] = new HashSet<uint>();
                            }

                            QuerySets[attribute][value].Add(queryCounter);
                        }
                    }
                }
            }

            RQFMax = ComputeRQFMax();

            // Calculate all QFs from the RQFs.
            foreach (string attr in Program.Attributes)
            {
                Dictionary<object, double> dict = new Dictionary<object, double>(RQFrequencies.Count);

                foreach (KeyValuePair<object, uint> rqf in RQFrequencies[attr])
                    dict[rqf.Key] = ((double)rqf.Value + 1) / (RQFMax + 1);
                
                QFrequencies.Add(attr, dict);
            }
        }
        
        public Dictionary<object, double> this[string attribute] => QFrequencies[attribute];

        /// <summary>
        /// Increae the count of a certain attribute value by a certain amount.
        /// </summary>
        /// <param name="attribute">The attribute to which the value belongs.</param>
        /// <param name="value">The value to increase the count of. Will be parsed to the correct type.</param>
        /// <param name="frequency">The amount to increase the count with.</param>
        private void IncreaseCount(string attribute, string value, uint frequency)
        {
            Dictionary<object, uint> dict = RQFrequencies[attribute];
            object parsed = ParseAttribute(attribute, value);

            if (dict.ContainsKey(parsed))
                dict[parsed] += frequency;
            else
                dict[parsed] = frequency;
        }

        /// <returns>Highest frequency of all attribute values</returns>
        private uint ComputeRQFMax()
        {
            // Previous code gave an error when .Values was empty (an attribute that was never asked for in the workload).
            uint max = 0;
            foreach (Dictionary<object, uint> dict in RQFrequencies.Values)
                foreach (uint freq in dict.Values)
                    if (freq > max)
                        max = freq;
            
            return max;
        }
    }
}
