using System;
using System.Data.SQLite;

namespace Preprocessing
{
    /// <summary>
    /// Some helper functions for preprocessing.
    /// </summary>
    public static class HelperFunctions
    {
        /// <summary>
        /// Create a connection to a database.
        /// </summary>
        /// <param name="name">The name/path of the database file, relative to the Preprocessing root, and assumed to have a .sqlite extension.</param>
        /// <returns>A connection to the database.</returns>
        public static SQLiteConnection CreateConnection(string name)
        {
            SQLiteConnection connection = new SQLiteConnection($"Data Source=../../{name}.sqlite;Version=3;");
            connection.Open();
            return connection;
        }
        
        /// <summary>
        /// Parse an attribute value to the correct type.
        /// </summary>
        /// <param name="attribute">The attribute that the value belongs to.</param>
        /// <param name="value">The value in string representation.</param>
        /// <returns>The value parsed to the correct type, based on the attribute, given as type object.</returns>
        /// <exception cref="ArgumentException">The attribute is not valid.</exception>
        public static object ParseAttribute(string attribute, string value)
        {
            switch (attribute)
            {
                // type double
                case "mpg":
                case "displacement":
                case "horsepower":
                case "weight":
                case "acceleration":
                    return double.Parse(value);
                // type long
                case "cylinders":
                case "model_year":
                case "origin":
                    return long.Parse(value);
                // type string
                case "brand":
                case "model":
                case "type":
                    return value;
            }

            throw new ArgumentException("The provided attribute name is not valid.", nameof(attribute));
        }
        
        private static (double stdDev, int n) GetStdDev(SQLiteConnection connection, string attribute)
        {
            string sql = $"SELECT {attribute} FROM autompg;";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            double sum = 0.0;
            double sumOfSquares = 0.0;
            int count = 0;

            while (reader.Read())
            {
                double value = Convert.ToDouble(reader[0]);
                sum += value;
                sumOfSquares += value * value;
                count++;
            }

            double avg = sum / count;
            double variance = sumOfSquares / count - avg * avg;

            return (Math.Sqrt(variance), count);
        }
    
        public static double CalculateBandwidth(SQLiteConnection connection, string attribute)
        {
            (double stdDev, int n) = GetStdDev(connection, attribute);
            return 1.06 * stdDev * Math.Pow(1.0 / n, -0.2);
        }
    }
}