﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace Preprocessing
{
    /// <summary>
    /// A class to calculate and store all inverted frequencies.
    /// </summary>
    public class AutoIdf
    {
        public Dictionary<string, Dictionary<object, uint>> Frequencies =
            new Dictionary<string, Dictionary<object, uint>>(Program.Attributes.Length);
        
        public Dictionary<string, Dictionary<object, float>> InvFrequencies =
            new Dictionary<string, Dictionary<object, float>>(Program.Attributes.Length);

        public Dictionary<string, double> Bandwidths = new Dictionary<string, double>(Program.Attributes.Length);

        public readonly uint Total = 0;
        
        private SQLiteConnection _connection;

        /// <summary>
        /// Create a new AutoIdf object from an existing AutoF object.
        /// </summary>
        /// <param name="f">The object containing frequencies.</param>
        /// <param name="connection">A connection to the source database.</param>
        public AutoIdf(SQLiteConnection connection)
        {
            _connection = connection;
            
            // Instantiate dictionaries
            foreach (string attr in Program.Attributes)
                Frequencies.Add(attr, new Dictionary<object, uint>());
            
            // Retrieve data from database and call IncreaseCount
            string selectAll = $"SELECT * FROM autompg;";
            SQLiteCommand command = new SQLiteCommand(selectAll, connection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                foreach (string attr in Program.Attributes)
                    IncreaseCount(attr, reader[attr]);
                Total++;
            }

            foreach (string attr in Program.Attributes)
                IdfFromF(attr);
        }
        
        public Dictionary<object, float> this[string attribute] => InvFrequencies[attribute];
        
        /// <summary>
        /// Increae the count of a certain attribute value.
        /// </summary>
        /// <param name="attribute">The attribute to which the value belongs.</param>
        /// <param name="value">The value to increase the count of.</param>
        private void IncreaseCount(string attribute, object value)
        {
            Dictionary<object, uint> dict = Frequencies[attribute];
            
            if (dict.ContainsKey(value))
                dict[value]++;
            else
                dict[value] = 1;
        }
        
        /// <summary>
        /// Calculate inverted frequencies and bandwidth for a given attribute, and add them to the dictionaries.
        /// </summary>
        /// <param name="attribute">Name of the attribute that will be handled.</param>
        private void IdfFromF(string attribute)
        {
            Dictionary<object, float> dict = new Dictionary<object, float>(Frequencies[attribute].Count);

            double h = 0;
            if (!(Frequencies[attribute].Keys.First() is string))
                h = HelperFunctions.CalculateBandwidth(_connection, attribute);

            Bandwidths[attribute] = h;
            
            foreach (KeyValuePair<object, uint> fkv in Frequencies[attribute])
            {
                switch (attribute)
                {
                    // Categorical
                    case "origin":
                    case "brand":
                    case "model":
                    case "type":
                        dict[fkv.Key] = (float)Math.Log((float)Total / fkv.Value);
                        break;
                    // Numerical
                    case "mpg":
                    case "cylinders":
                    case "displacement":
                    case "horsepower":
                    case "weight":
                    case "acceleration":
                    case "model_year":
                        break;
                }
            }

            InvFrequencies.Add(attribute, dict);
        }
    }
}