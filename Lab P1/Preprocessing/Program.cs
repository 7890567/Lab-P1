﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using static Preprocessing.HelperFunctions;

namespace Preprocessing
{
    internal static class Program
    {
        public static string[] Attributes = new string[] { "mpg", "cylinders", "displacement",
            "horsepower", "weight", "acceleration", "model_year", "origin", "brand", "model", "type" };

        // Got help from this tutorial https://web.archive.org/web/20190910153157/http://blog.tigrangasparian.com/2012/02/09/getting-started-with-sqlite-in-c-part-one/
        
        public static void Main(string[] args)
        {
            // CreateOriginalDb();
            
            // Setup
            SQLiteConnection originalDb = CreateConnection("original");
            AutoIdf autoIdf = new AutoIdf(originalDb);
            QFSimilarity qfSimilarity = new QFSimilarity("../../workload.txt");
            
            // Create meta database
            CreateMetaDb(autoIdf, qfSimilarity);
            
            originalDb.Close();
        }

        private static void CreateMetaDb(AutoIdf autoIdf, QFSimilarity qfSimilarity)
        {
            // Delete old files if they exist
            File.Delete("../../metadb.sqlite");
            File.Delete("../../metadb.txt");
            File.Delete("../../metafill.txt");
            
            // Setup database
            SQLiteConnection connection = new SQLiteConnection("Data Source=../../metadb.sqlite;Version=3;New=True;");
            connection.Open();
            LinkedList<string> allCreation = new LinkedList<string>(),
                allFill = new LinkedList<string>();
            
            // Create IDF tables
            foreach (string attr in Attributes)
            {
                switch (attr)
                {
                    // type real
                    case "mpg":
                    case "displacement":
                    case "horsepower":
                    case "weight":
                    case "acceleration":
                        allCreation.AddLast($"CREATE TABLE {attr}_idf (value REAL, idf REAL, PRIMARY KEY (value));");
                        foreach (KeyValuePair<object, float> idfs in autoIdf[attr])
                            allFill.AddLast($"INSERT INTO {attr}_idf VALUES ({((double)idfs.Key).ToString(CultureInfo.InvariantCulture)}, {idfs.Value.ToString(CultureInfo.InvariantCulture)});");
                        break;
                    // type integer
                    case "cylinders":
                    case "model_year":
                    case "origin":
                        allCreation.AddLast($"CREATE TABLE {attr}_idf (value INTEGER, idf REAL, PRIMARY KEY (value));");
                        foreach (KeyValuePair<object, float> idfs in autoIdf[attr])
                            allFill.AddLast($"INSERT INTO {attr}_idf VALUES ({idfs.Key}, {idfs.Value.ToString(CultureInfo.InvariantCulture)});");
                        break;
                    // type text
                    case "brand":
                    case "model":
                    case "type":
                        allCreation.AddLast($"CREATE TABLE {attr}_idf (value TEXT, idf REAL, PRIMARY KEY (value));");
                        foreach (KeyValuePair<object, float> idfs in autoIdf[attr])
                            allFill.AddLast($"INSERT INTO {attr}_idf VALUES ('{idfs.Key}', {idfs.Value.ToString(CultureInfo.InvariantCulture)});");
                        break;
                }

                allFill.AddLast(string.Empty);
            }
            
            // Create QF tables
            foreach (string attr in Attributes)
            {
                switch (attr)
                {
                    // type real
                    case "mpg":
                    case "displacement":
                    case "horsepower":
                    case "weight":
                    case "acceleration":
                        allCreation.AddLast($"CREATE TABLE {attr}_qf (value REAL, qf REAL, PRIMARY KEY (value));");
                        foreach (KeyValuePair<object, double> qfs in qfSimilarity[attr])
                            allFill.AddLast($"INSERT INTO {attr}_qf VALUES ({((double)qfs.Key).ToString(CultureInfo.InvariantCulture)}, {qfs.Value.ToString(CultureInfo.InvariantCulture)});");
                        break;
                    // type integer
                    case "cylinders":
                    case "model_year":
                    case "origin":
                        allCreation.AddLast($"CREATE TABLE {attr}_qf (value INTEGER, qf REAL, PRIMARY KEY (value));");
                        foreach (KeyValuePair<object, double> qfs in qfSimilarity[attr])
                            allFill.AddLast($"INSERT INTO {attr}_qf VALUES ({qfs.Key}, {qfs.Value.ToString(CultureInfo.InvariantCulture)});");
                        break;
                    // type text
                    case "brand":
                    case "model":
                    case "type":
                        allCreation.AddLast($"CREATE TABLE {attr}_qf (value TEXT, qf REAL, PRIMARY KEY (value));");
                        foreach (KeyValuePair<object, double> qfs in qfSimilarity[attr])
                            allFill.AddLast($"INSERT INTO {attr}_qf VALUES ('{qfs.Key}', {qfs.Value.ToString(CultureInfo.InvariantCulture)});");
                        break;
                }

                allFill.AddLast(string.Empty);
            }
            
            // Create bandwidth table
            allCreation.AddLast("CREATE TABLE bandwidths (attribute TEXT, bandwidth REAL, PRIMARY KEY (attribute));");
            foreach (string attr in Attributes)
                allFill.AddLast($"INSERT INTO bandwidths VALUES ('{attr}', {autoIdf.Bandwidths[attr].ToString(CultureInfo.InvariantCulture)});");
            allFill.AddLast(string.Empty);
            
            // Create W tables (of querysets with value in IN clause)
            foreach (string attr in Attributes)
            {
                switch (attr)
                {
                    // type text
                    case "brand":
                    case "model":
                    case "type":
                        allCreation.AddLast($"CREATE TABLE {attr}_w  (value TEXT, querysets TEXT, PRIMARY KEY (value))");
                        foreach (KeyValuePair<string,HashSet<uint>> querySet in qfSimilarity.QuerySets[attr])
                            allFill.AddLast($"INSERT INTO {attr}_w VALUES ('{querySet.Key}', '{string.Join(",", querySet.Value)}');");
                        break;
                }
                
                allFill.AddLast(string.Empty);
            }
            
            // Miscelaneous values to use at query time
            allCreation.AddLast("CREATE TABLE misc (totaln INTEGER, rqfmax INTEGER);");
            allFill.AddLast($"INSERT INTO misc VALUES ({autoIdf.Total}, {qfSimilarity.RQFMax});");
            
            // Execute queries and write to output file
            using (StreamWriter sw = File.CreateText("../../metadb.txt"))
                foreach (string sql in allCreation)
                {
                    new SQLiteCommand(sql, connection).ExecuteNonQuery();
                    sw.WriteLine(sql);
                    sw.WriteLine();
                }
            
            using (StreamWriter sw = File.CreateText("../../metafill.txt"))
                foreach (string sql in allFill)
                {
                    new SQLiteCommand(sql, connection).ExecuteNonQuery();
                    sw.WriteLine(sql);
                }
            
            connection.Close();
        }

        // Meant to be used only once, to convert the given .sql file into an sqlite database file
        private static void CreateOriginalDb()
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=../../original.sqlite;Version=3;New=True;");
            connection.Open();

            string sql = File.ReadAllText("../../autompg.sql");
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.ExecuteNonQuery();
            
            connection.Close();
        }
    }
}